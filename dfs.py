"""
https://en.wikipedia.org/wiki/Depth-first_search
"""


import typing


class DFS:

    def __init__(self, graph):
        self.graph = graph
        self.path = []
        self.visited = [False for _ in range(len(self.graph))]

    def __str__(self):
        pass

    def search(self, target: int):
        if target < 0 or target >= len(self.graph):
            return f"Node {target} does not exist."

        self.visited[0] = True
        self.path.append(0)

        return self.__search(0, target)

    def __search(self, source: int, target: int):
        if source == target:
            return self.path

        if len(self.path) == len(self.graph):
            return f"Node {target} not found."

        for i in range(len(self.graph)):
            if self.graph[source][i] and not self.visited[i]:
                self.visited[i] = True
                self.path.append(i)

                if i == target:
                    return self.path

                return self.__search(i, target)

        return self.path


if __name__ == "__main__":
    graph = [
        [0, 0, 0, 0, 1],
        [0, 0, 1, 0, 0],
        [0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1],
        [1, 0, 0, 1, 0]
    ]

    print(DFS(graph).search(0))
    print(DFS(graph).search(1))
    print(DFS(graph).search(2))
    print(DFS(graph).search(3))
    print(DFS(graph).search(4))
    print(DFS(graph).search(-1))
    print(DFS(graph).search(6))
