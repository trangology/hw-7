"""
https://en.wikipedia.org/wiki/Breadth-first_search
"""


import typing


class BFS:

    def __init__(self, graph):
        self.graph = graph
        self.path = []
        self.visited = [False for _ in range(len(self.graph))]

    def __str__(self):
        pass

    def search(self, target: int):
        return self.__search(target)

    def __search(self, target: int):
        if target < 0 or target >= len(self.graph):
            return f"Node {target} does not exist."

        self.visited[0] = True
        self.path.append(0)

        if target == 0:
            return self.path

        i = 0
        while i < len(self.graph):
            for j in range(len(self.graph)):
                if self.graph[i][j] and not self.visited[j]:
                    self.visited[j] = True
                    self.path.append(j)
                    if j == target:
                        return self.path
            i = i + 1

        return self.path


if __name__ == "__main__":
    graph = [
        [0, 1, 0, 1, 0],
        [1, 0, 1, 0, 0],
        [1, 1, 0, 0, 0],
        [1, 0, 0, 0, 1],
        [0, 0, 0, 1, 0]
    ]
    
    print(BFS(graph).search(0))
    print(BFS(graph).search(1))
    print(BFS(graph).search(2))
    print(BFS(graph).search(3))
    print(BFS(graph).search(4))
    print(BFS(graph).search(-1))
    print(BFS(graph).search(6))
