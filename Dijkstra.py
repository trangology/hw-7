"""
Time complexity: O(v^2)

https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
"""


class Dijkstra:

    INFINITY = 1000000000
    
    def __init__(self, graph):
        self.graph = graph
        self.distances = [self.INFINITY for _ in range(len(self.graph))]
        self.visited = []
        self.prev_vertices = [None for _ in range(len(self.graph))]
        self.shortest_paths = []


    def __str__(self):
        pass


    def __repr__(self):
        pass


    def find_shortest_paths(self, source: int) -> list:
        return self.__find_shortest_paths(source)


    def __find_shortest_paths(self, source: int) -> list:
        if source < 0 or source >= len(self.graph):
            return f"Vertex '{source}' does not exist."
        
        self.distances[source] = 0

        while len(self.visited) < len(self.graph):
            cur_vertex = self.__find_next_vertex()
            self.visited.append(cur_vertex)

            for neighbor in range(len(self.graph)):
                if neighbor not in self.visited and self.graph[cur_vertex][neighbor]:
                    distance = self.distances[cur_vertex] + self.graph[cur_vertex][neighbor]
                    if 0 < distance < self.distances[neighbor]:
                        self.distances[neighbor] = distance
                        self.prev_vertices[neighbor] = cur_vertex
            
        return self.__get_shortest_paths(source)

    
    def __find_next_vertex(self):
        min_distance = self.INFINITY + 1
        next_vertex = -1
        for i in range(len(self.distances)):
            if self.distances[i] < min_distance and i not in self.visited:
                min_distance = self.distances[i]
                next_vertex = i
        return next_vertex


    def __get_shortest_paths(self, source: int) -> list:
        """ Get all the shortest paths between `source` and every other."""

        for vertex in range(len(self.graph)):
            if vertex is not source:
                path = self.__get_shortest_path(source, vertex)
                self.shortest_paths.append(path)

        return self.shortest_paths


    def __get_shortest_path(self, source: int, target: int) -> list:
        """Get 1 shortest path between vertices `source` and `target`."""

        vertices = [target]
        prev_vertex = self.prev_vertices[target]
        while prev_vertex is not None and prev_vertex is not source:
            vertices.append(prev_vertex)
            prev_vertex = self.prev_vertices[prev_vertex]
        
        vertices.append(source)
        return list(reversed(vertices))


if __name__ == "__main__":
    my_graph = [
        [0, 4, 0, 0, 5],
        [4, 0, 3, 2, 0],
        [0, 3, 0, 1, 0],
        [0, 2, 1, 0, 7],
        [5, 0, 0, 7, 0]
    ]

    path0 = Dijkstra(my_graph).find_shortest_paths(0)
    path1 = Dijkstra(my_graph).find_shortest_paths(1)
    path2 = Dijkstra(my_graph).find_shortest_paths(2)
    path3 = Dijkstra(my_graph).find_shortest_paths(3)
    path4 = Dijkstra(my_graph).find_shortest_paths(4)
    path5 = Dijkstra(my_graph).find_shortest_paths(5)
    path6 = Dijkstra(my_graph).find_shortest_paths(-1)
    
    print(f"Shortest paths between vertex 0 and every other: {path0}")
    print(f"Shortest paths between vertex 1 and every other: {path1}")
    print(f"Shortest paths between vertex 2 and every other: {path2}")
    print(f"Shortest paths between vertex 3 and every other: {path3}")
    print(f"Shortest paths between vertex 4 and every other: {path4}")

    print(path5)
    print(path6)
